# RAVEN GPS

## Installation
Install the serial library
```
$ sudo apt install ros-kinetic-serial
```

Create catkin workspace
```
$ mkdir -p gps_ws/src
$ cd gps_ws
$ catkin_make
```

Clone code, build, and then source
```
$ cd src
$ git clone https://gitlab.com/CU-RAVEN/ublox.git
$ cd ..
$ catkin_make
$ source devel/setup.bash
```

## Running the code
First terminal
```
$ roscore
````

Connect ublox receiver and run
```
$ rosrun ublox ublox_node
```