#include <string>

#include "ros/ros.h"
#include "mavros_msgs/HilGPS.h"
#include "std_msgs/Float64MultiArray.h"

#include "ublox/ublox.h"

using std::string;

class UbloxNode
{
public:
  UbloxNode() : nh_("~"), port_("") {
    this->getROSParameters();
    this->configureROSCommunications();
  }
  ~UbloxNode() {

  }

  void handle_NavPVT(ublox::NavPVT& nav_pvt, double& time_stamp) {
    mavros_msgs::HilGPS msg;
    msg.header.stamp = ros::Time(time_stamp);
    msg.header.frame_id = "/ublox";

    msg.geo.latitude = nav_pvt.latitude / 1e7f;
    msg.geo.longitude = nav_pvt.longitude / 1e7f;
    msg.geo.altitude = nav_pvt.altitude / 1e3f;

    msg.eph = nav_pvt.hAcc / 1000.0f;
    msg.epv = nav_pvt.vAcc / 1000.0f;

    msg.vel = nav_pvt.ground_speed / 10.0f;

    // TODO CHECK VALUES
    msg.vn = nav_pvt.velocity_north / 10.0f;
    msg.ve = nav_pvt.velocity_east / 10.0f;
    msg.vd = nav_pvt.velocity_down / 10.0f;

    msg.cog = nav_pvt.heading_motion / 1000.0f;

    msg.fix_type = nav_pvt.gpsFix;

    msg.satellites_visible = nav_pvt.numSV;

    //this->navpvt_pub_.publish(msg);
  }

  void handle_NavRelPosNed(ublox::NavRelPosNed& nav_relposned, double& time_stamp) {
    std_msgs::Float64MultiArray msg;
    msg.data.resize(4);
    msg.data[0] = ros::Time(time_stamp).toSec();
    msg.data[1] = nav_relposned.relative_position_east/100.0f + nav_relposned.rel_pos_Hp_east/10000.0f;
    msg.data[2] = nav_relposned.relative_position_north/100.0f + nav_relposned.rel_pos_Hp_north/10000.0f;
    msg.data[3] = -1.0*nav_relposned.relative_position_down/100.0f + -1.0*nav_relposned.rel_pos_Hp_down/10000.0f;
    //msg.data[4] = nav_relposned.accuracy_north/10000.0f;
    //msg.data[5] = nav_relposned.accuracy_east/10000.0f;
    //msg.data[6] = nav_relposned.accuracy_down/10000.0f;

    this->navrelposned_pub_.publish(msg);
  }

  void setup() {
    bool connect_result = false;
    try {
      connect_result = ublox_.Connect(port_, baudrate_);
    } catch (const std::exception &e) {
      ROS_ERROR_STREAM("Error connecting to the uBlox on port `"
                       << port_ << "`: " << e.what());
    }
    ublox_.set_nav_pvt_callback(
      boost::bind(&UbloxNode::handle_NavPVT, this, _1, _2) 
    ); 
    ublox_.set_nav_rel_pos_ned_callback(
      boost::bind(&UbloxNode::handle_NavRelPosNed, this, _1, _2)
    );
  }

  void getROSParameters() {
    nh_.param<std::string>("port", port_, "/dev/ttyACM0");
    nh_.param<int>("baudrate", baudrate_, 19200);
  }

  void configureROSCommunications() {
    navpvt_pub_ = nh_.advertise<mavros_msgs::HilGPS>("/mavros/hil/gps", 1000);
    navrelposned_pub_ = nh_.advertise<std_msgs::Float64MultiArray>("/relative_position", 1000);
  }

private:
  ros::NodeHandle nh_;
  string port_;
  int baudrate_;
  ros::Publisher navpvt_pub_;
  ros::Publisher navrelposned_pub_;

  ublox::Ublox ublox_;
};

int main (int argc, char **argv) {
  ros::init(argc, argv, "ublox_node");

  UbloxNode ublox_node;
  ublox_node.setup();

  ros::spin();

  return 0;
}
